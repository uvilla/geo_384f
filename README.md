#  GEO_384F: FEniCS materials 

Welcome to the `git` repository of the course **GEO 384F: Finite Element Method in Geophysics**, Prof. Omar Ghattas, UT Austin, Fall 2016.

Here you will find useful materials regarding [FEniCS](http://fenicsproject.org/), the high-level powerful finite element software toolkit we'll be using in class.

FEniCS is a powerful, open-source suite of tools for automated solution of PDEs using finite elements. Part of the power for FEniCS is the ease with which one can create FE solvers by describing PDEs using weak forms in nearly-mathematical notation.

In this class, we will use `FEniCS 2016.1` from the Python interface. Note that some of the material may not work in previous versions of FEniCS.

A good starting point for new users are the existing [demos](https://fenicsproject.org/documentation/dolfin/2016.1.0/python/demo/index.html). It is the most up-to-date resource (`FEniCS 2016.1`) and it will help you get quickly up and running with solving differential equations in FEniCS.

See the [wiki](https://bitbucket.org/uvilla/geo_384f/wiki/Home) for more information. 