{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Linear elasticity\n",
    "\n",
    "Here we briefly derive the equation of linear isotropic elasticity.\n",
    "\n",
    "Let $\\boldsymbol{u} \\in \\mathbb{R}^2$ be the displacement field. The strain tensor $\\varepsilon$ is defined as\n",
    "$$ \\varepsilon = \\frac{1}{2}\\left(\\nabla \\boldsymbol{u} + \\nabla \\boldsymbol{u}^T \\right) = \\begin{bmatrix} \\frac{\\partial u_x}{\\partial x} & \\frac{1}{2}\\left(\\frac{\\partial u_y}{\\partial x} + \\frac{\\partial u_x}{\\partial y} \\right)\\\\ \\frac{1}{2}\\left(\\frac{\\partial u_y}{\\partial x} + \\frac{\\partial u_x}{\\partial y} \\right) & \\frac{\\partial u_y}{\\partial y} \\end{bmatrix}. $$\n",
    "\n",
    "The constituve relation of linear isotropic elasticity between the strain tenson $\\varepsilon$ and the stress tensor is\n",
    "$$ \\sigma = 2 \\mu \\varepsilon + \\lambda \\operatorname{tr}(\\varepsilon) I $$\n",
    "\n",
    "$$ - \\nabla \\cdot \\sigma = \\boldsymbol{f} $$\n",
    "\n",
    "Note $ \\nabla \\cdot \\sigma = \\begin{bmatrix}\\frac{\\partial \\sigma_{xx}}{\\partial x} + \\frac{\\partial \\sigma_{xy}}{\\partial y} \\\\  \\frac{\\partial \\sigma_{xy}}{\\partial x} + \\frac{\\partial \\sigma_{yy}}{\\partial y} \\end{bmatrix}$\n",
    "\n",
    "Displacement form of elasticity equation\n",
    "\n",
    "$$ \\nabla \\cdot \\left[\\mu\\, \\left(\\nabla \\boldsymbol{u} + \\nabla \\boldsymbol{u}^T \\right) + \\lambda\\,\\left(\\nabla \\cdot \\boldsymbol{u} \\right) I \\right] = \\boldsymbol{f} $$\n",
    "\n",
    "$ u = u_D$ on $\\Gamma_D$ and $\\sigma \\cdot \\boldsymbol{n} = \\boldsymbol{t}_n$ on $\\Gamma_N$.\n",
    "\n",
    "## Weak form\n",
    "\n",
    "$$ \\int_\\Omega \\boldsymbol{v} \\left[ - \\nabla \\cdot \\sigma - \\boldsymbol{f} \\right] dxdy = 0 \\quad \\forall \\boldsymbol{v} \\in V $$\n",
    "\n",
    "Integration by part of the nonconforming term gives\n",
    "\n",
    "$$ \\int_\\Omega \\left[\\nabla\\boldsymbol{v} : \\sigma -  \\boldsymbol{v}\\cdot \\boldsymbol{f} \\right] dx\\,dy +\n",
    "\\int_\\Gamma \\boldsymbol{v} \\cdot (\\sigma \\boldsymbol{n} ) ds = 0. $$\n",
    "\n",
    "Recall that $\\boldsymbol{v} = \\boldsymbol{0}$ on $\\Gamma_D$ and $\\sigma \\cdot \\boldsymbol{n} = \\boldsymbol{t}_n$ on $\\Gamma_N$, and substituting $\\sigma$ we write\n",
    "\n",
    "$$ \\int_\\Omega \\mu (\\nabla \\boldsymbol{v}) : \\left(\\nabla \\boldsymbol{u} + \\nabla \\boldsymbol{u}^T \\right) dx\\,dy +\n",
    "\\int_\\Omega \\lambda (\\nabla\\cdot \\boldsymbol{v})(\\nabla\\cdot \\boldsymbol{u})\\, dx\\,dy = \\int_{\\Omega} \\boldsymbol{f}\\cdot\\boldsymbol{v} \\, dx\\,dy + \\int_{\\Gamma_N} \\boldsymbol{v} \\cdot \\boldsymbol{t}_n ds $$.\n",
    "\n",
    "Finally, note that:\n",
    "$$ (\\nabla \\boldsymbol{v}) : \\left(\\nabla \\boldsymbol{u} + \\nabla \\boldsymbol{u}^T \\right) = \\frac{1}{2}\\left(\\nabla \\boldsymbol{v} + \\nabla \\boldsymbol{v}^T \\right) : \\left(\\nabla \\boldsymbol{u} + \\nabla \\boldsymbol{u}^T \\right), $$\n",
    "because of symmetry.\n",
    "\n",
    "We end up with \n",
    "\n",
    "Find $ \\boldsymbol{u} \\in V_{u_D}$ such that\n",
    "$$ \\int_\\Omega \\frac{1}{2}\\mu \\left(\\nabla \\boldsymbol{v} + \\nabla \\boldsymbol{v}^T \\right) : \\left(\\nabla \\boldsymbol{u} + \\nabla \\boldsymbol{u}^T \\right) dx\\,dy +\n",
    "\\int_\\Omega \\lambda (\\nabla\\cdot \\boldsymbol{v})(\\nabla\\cdot \\boldsymbol{u})\\, dx\\,dy = \\int_{\\Omega} \\boldsymbol{f}\\cdot\\boldsymbol{v} \\, dx\\,dy + \\int_{\\Gamma_N} \\boldsymbol{v} \\cdot \\boldsymbol{t}_n ds \\quad \\boldsymbol{v} \\in V_{0}.$$\n",
    "\n",
    "## Finite element discretization\n",
    "\n",
    "$ \\boldsymbol{u} \\mapsto \\boldsymbol{u}_h = \\begin{bmatrix}\\sum_{j=1}^N \\phi_j u_{xj} \\\\  \\sum_{j=1}^N \\phi_j u_{yj} \\end{bmatrix}$ and $\\boldsymbol{v}\\mapsto \\boldsymbol{v}_h = \\left\\{ \\begin{bmatrix} \\phi_i \\\\  0 \\end{bmatrix}, \\begin{bmatrix} 0 \\\\ \\phi_i \\end{bmatrix}\\right\\}_{i=1}^N$.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 1. Imports\n",
    "We import the following Python packages:\n",
    "\n",
    "- `dolfin` is the python interface to FEniCS.\n",
    "- `numpy` is a fundamental package for scientific computing (arrays and linear algebra)\n",
    "- `matplotlib` is a plotting library that produces figure similar to the Matlab ones."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from dolfin import *\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 2. Define the mesh and the finite element space\n",
    "The domain $\\Omega$ is a rectangle with height `height` and length `length`.\n",
    "We define a triangulation (mesh) of $\\Omega$ with `nx` elements in the horizontal direction and `ny` elements in the vertical direction.\n",
    "\n",
    "We also define the vectorial finite element space $V_h$. Each component of $V_h$ (one for the horizontal and one for the vertical displacements) is a space of globally continuos functions that are piecewise polinomial (of degree $d$) on the elements of the mesh."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "height = 1.0\n",
    "length = 5.0\n",
    "nx = 100\n",
    "ny = 10\n",
    "d  = 1 \n",
    "\n",
    "mesh = RectangleMesh(Point(0.,0), Point(length, height), nx,ny)\n",
    "Vh = VectorFunctionSpace(mesh, \"CG\", 1)\n",
    "print \"Number of dofs\", Vh.dim()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 3. Define the Dirichlet boundary condition\n",
    "We define the Dirichlet boundary condition $\\boldsymbol{u} = \\boldsymbol{0}$ on the left boundary on the rectangle."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def boundary_d(x, on_boundary):\n",
    "    return x[0] < DOLFIN_EPS  and on_boundary\n",
    "\n",
    "u_d  = Constant((0.,0.))\n",
    "bcs = [DirichletBC(Vh, u_d, boundary_d)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 4. Define the variational problem\n",
    "\n",
    "We write the variational problem $a(u_h, v_h) = F(v_h). Here, the bilinear form $a$ and the linear form $L$ are defined as\n",
    "\n",
    "- $a(\\boldsymbol{u}_h, \\boldsymbol{v}_h) := \\int_\\Omega \\frac{1}{2}\\mu\\, (\\nabla \\boldsymbol{u}_h + \\nabla \\boldsymbol{u}_h^T):(\\nabla \\boldsymbol{v}_h + \\nabla \\boldsymbol{v}_h^T)\\, dx + \\int_\\Omega \\lambda (\\nabla \\cdot \\boldsymbol{u}_h) \\, (\\nabla \\cdot \\boldsymbol{v}_h)$\n",
    "- $L(\\boldsymbol{v}_h) := \\int_\\Omega \\boldsymbol{f} \\cdot \\boldsymbol{v}_h \\, dx$.\n",
    "\n",
    "$\\boldsymbol{u}_h$ denotes the trial function and $\\boldsymbol{v}_h$ denotes the test function. \n",
    "The Lam&egrave; constants $mu$ and $\\lambda$ are computed from the Young modulus `E` and the Poisson coefficient `nu`. The vector $\\boldsymbol{f} = [0; -9.8]$ denotes the body forces (gravity)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "uh = TrialFunction(Vh)\n",
    "vh = TestFunction(Vh)\n",
    "\n",
    "E = 1.0e4\n",
    "nu = 0.3\n",
    "\n",
    "mu  = Constant(E/(2.0*(1.0 + nu)))\n",
    "lda = Constant(E*nu/((1.0 + nu)*(1.0 - 2.0*nu)))  \n",
    "f   = Constant((0,-9.8))\n",
    "\n",
    "a = Constant(0.5)*mu*inner(grad(uh) + grad(uh).T, grad(vh) + grad(vh).T)*dx \\\n",
    "    + lda*div(uh)*div(vh)*dx\n",
    "L = inner(f,vh)*dx"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### 5. Assemble and solve the finite element discrete problem\n",
    "\n",
    "We now assemble the finite element stiffness matrix $A$ and the right hand side vector $b$. Dirichlet boundary conditions are applied at the end of the finite element assembly procedure and before solving the resulting linear system of equations.\n",
    "\n",
    "We then show the displacement $\\boldsymbol{u}_h$ as a vector field, the horizontal and vertical component separately, and finally the deformed mesh."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "A, b = assemble_system(a, L, bcs)\n",
    "uh = Function(Vh)\n",
    "solve(A, uh.vector(), b)\n",
    "\n",
    "figsize = (2*length, 2*height)\n",
    "plt.figure(figsize = figsize)\n",
    "plot(uh, title=\"Displacement\")\n",
    "plt.figure(figsize = figsize)\n",
    "plot(uh.sub(0), title=\"Horizontal Displacement\")\n",
    "plt.figure(figsize = figsize)\n",
    "plot(uh.sub(1), title=\"Vertical Displacement\")\n",
    "\n",
    "coordinates = mesh.coordinates()\n",
    "out = uh.compute_vertex_values()\n",
    "coordinates[:,0] += out[0:coordinates.shape[0]]\n",
    "coordinates[:,1] += out[coordinates.shape[0]: ]\n",
    "plt.figure(figsize = (2*length, 4*height))\n",
    "plot(mesh, title=\"Deformed mesh\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.10"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
