# # Equivalence between energy formulation and BVP formulation
# 
# We let $\Omega := (0,1)$, and we define the spaces
# $$ V := \left\{ v \in H^1(\Omega) \;|\; v(0) = a \text{ and } v(1) = b \right\}, \quad V^{(0)} := \left\{ v \in H^1(\Omega) \;|\; v(0) = v(1) = 0 \right\},$$ 
# where $a = 1$ and $b = 0$.
# 
# We also define the energy functional $\Pi(u)$ as
# $$ \Pi(u) = \frac{1}{2}\int_0^1 k(x) \left( \frac{du}{dx} \right)^2 dx - \int_0^1 f(x) \, u\, dx \quad \forall u \in V$$
# where $k(x) = 1 + x^2$ and $f(x) = 0$.
# 
# Then, the minimizer $u$ of the energy functional $\Pi$ is the solution of the boundary value problem:
# 
# Find $ u \in V $ such that:
# $$ \int_0^1 k(x) \frac{du}{dx}\, \frac{dv}{dx} \, dx = \int_0^1 f(x)\,v\,dx \quad \forall v \in V^{(0)}.$$
# 

# Step 1. Import modules, define the mesh and the finite element space
from dolfin import *

n = 10
mesh = UnitIntervalMesh(n)
d = 1
Vh = FunctionSpace(mesh, "CG", d)

print "Number of degrees of freedom:", Vh.dim()


# Step 2. Definition of the Dirchlet BC and coefficients
def left_boundary(x, on_boundary):
    return near(x[0], 0.) and on_boundary
def right_boundary(x, on_boundary):
    return near(x[0],1.) and on_boundary

a = Constant(1.)
b = Constant(0.)
left_bc = DirichletBC(Vh, a, left_boundary)
right_bc = DirichletBC(Vh, b, right_boundary)

bcs = [left_bc, right_bc]

k = Expression("1.0 + x[0]*x[0]", degree = 2)
f = Constant(0.)

# Step 3. Ritz method

uh = Function(Vh)
Pi = .5*k*inner(grad(uh), grad(uh))*dx - inner(f,uh)*dx
gradPi = derivative(Pi, uh)
solve(gradPi == 0, uh, bcs)

print "Minimal energy functional value: ", assemble(Pi)
File("Ritz.pvd") << uh


# Step 4. Finite element solution
uh = TrialFunction(Vh)
vh = TestFunction(Vh)

Aform = k*inner(grad(uh), grad(vh))*dx
bform = inner(f,vh)*dx

A,b = assemble_system(Aform, bform, bcs)

uh_FE = Function(Vh)

solve(A, uh_FE.vector(), b)

print "Energy functional value", assemble(.5*k*inner(grad(uh_FE), grad(uh_FE))*dx - inner(f,uh_FE)*dx)
File("FEM.pvd") << uh_FE

