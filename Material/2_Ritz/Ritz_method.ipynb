{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Equivalence between energy formulation and BVP formulation\n",
    "\n",
    "In this notebook, we will show the equivalence between the energy minimization formulation and the boundary value problem formulation.\n",
    "\n",
    "More specifically, we let $\\Omega := (0,1)$, and we define the spaces\n",
    "$$ V := \\left\\{ v \\in H^1(\\Omega) \\;|\\; v(0) = a \\text{ and } v(1) = b \\right\\}, \\quad V^{(0)} := \\left\\{ v \\in H^1(\\Omega) \\;|\\; v(0) = v(1) = 0 \\right\\},$$ \n",
    "where $a = 1$ and $b = 0$.\n",
    "\n",
    "We also define the energy functional $\\Pi(u)$ as\n",
    "$$ \\Pi(u) = \\frac{1}{2}\\int_0^1 k(x) \\left( \\frac{du}{dx} \\right)^2 dx - \\int_0^1 f(x) \\, u\\, dx \\quad \\forall u \\in V$$\n",
    "where $k(x) = 1 + x^2$ and $f(x) = 0$.\n",
    "\n",
    "Then, the minimizer $u$ of the energy functional $\\Pi$ is the solution of the boundary value problem:\n",
    "\n",
    "Find $ u \\in V $ such that:\n",
    "$$ \\int_0^1 k(x) \\frac{du}{dx}\\, \\frac{dv}{dx} \\, dx = \\int_0^1 f(x)\\,v\\,dx \\quad \\forall v \\in V^{(0)}.$$\n",
    "\n",
    "For completeness, we also show the strong form of the boundary value problem\n",
    "$$\n",
    "\\left\\{\n",
    "\\begin{array}{ll}\n",
    "- \\frac{d}{dx}\\left( k(x) \\frac{du}{dx} \\right) = f & \\forall x \\in (0,1) \\\\\n",
    "u(0) = a, \\, u(1) = b & {}\n",
    "\\end{array}\n",
    "\\right.\n",
    "$$\n",
    "\n",
    "## The Ritz method \n",
    "\n",
    "We let $V_h \\subset V$ be a finite dimensional subspace of $V$ and then solve the minimization problem\n",
    "$$ \\min_{u_h \\in V_h} \\Pi(u_h) $$\n",
    "\n",
    "## The finite element method\n",
    "We let $V_h \\subset V$, $V_h^{(0)} \\subset V^{{0}}$ be finite dimensional subspaces of (respectively) $V$ and $V^{(0)}$. Then we solve\n",
    "\n",
    "Find $ u_h \\in V_h $ such that:\n",
    "$$ \\int_0^1 k(x) \\frac{du_h}{dx}\\, \\frac{dv_h}{dx} \\, dx = \\int_0^1 f(x)\\,v_h\\,dx \\quad \\forall v_h \\in V_h^{(0)}.$$"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 1. Import modules, define the mesh and the finite element space\n",
    "\n",
    "The domain $\\Omega = (0,1)$ is dicretized using a mesh with $n$ elements and $n+1$ vertices $\\left( h = \\frac{1}{n}\\right)$ .\n",
    "The finite element space $V_h$ consists of globally continuous functions that are piecewise polynomials of degree $d$ over each element of the mesh."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "from dolfin import *\n",
    "%matplotlib inline\n",
    "\n",
    "n = 10\n",
    "mesh = UnitIntervalMesh(n)\n",
    "d = 1\n",
    "Vh = FunctionSpace(mesh, \"CG\", d)\n",
    "\n",
    "print \"Number of degrees of freedom:\", Vh.dim()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 2. Definition of the Dirchlet BC and coefficients\n",
    "\n",
    "To define each Dirichlet boundary condition, we construct an object of type `DirichletBC`. The inputs are the finite element space $V_h$, an expression for the boundary value, and a function defining the location of the boundary.\n",
    "\n",
    "We then collect all the Dirichlet boundary conditions in a list `bcs` that will be passed to the assembly and solve routines.\n",
    "\n",
    "As usual, the coefficients $k$ and $f$ are defined using the `Expression` (for space dependent coefficients) or `Constant` (for constant coefficients) classes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "def left_boundary(x, on_boundary):\n",
    "    return near(x[0], 0.) and on_boundary\n",
    "def right_boundary(x, on_boundary):\n",
    "    return near(x[0],1.) and on_boundary\n",
    "\n",
    "a = Constant(1.)\n",
    "b = Constant(0.)\n",
    "left_bc = DirichletBC(Vh, a, left_boundary)\n",
    "right_bc = DirichletBC(Vh, b, right_boundary)\n",
    "\n",
    "bcs = [left_bc, right_bc]\n",
    "\n",
    "k = Expression(\"1.0 + x[0]*x[0]\", degree = 2)\n",
    "f = Constant(0.)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 3. Ritz method\n",
    "\n",
    "Here we use the dolfin function `derivative` to compute the first variation of the energy functional with respect to the finite element function $u_h$. Note that, since the energy functional $\\Pi$ is a quadratic functional in $u_h$, the first variation is a linear form in $u_h$.\n",
    "\n",
    "To find the minimizer of $\\Pi$ we apply first order optimality conditions and we set the first variation of $\\Pi$ equal to zero. This step is done using the dolfin function `solve`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "uh = Function(Vh)\n",
    "Pi = .5*k*inner(grad(uh), grad(uh))*dx - inner(f,uh)*dx\n",
    "gradPi = derivative(Pi, uh)\n",
    "solve(gradPi == 0, uh, bcs)\n",
    "\n",
    "print \"Minimal energy functional value: \", assemble(Pi)\n",
    "plot(uh, title=\"Ritz solution\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Step 4. Finite element solution\n",
    "\n",
    "Here, we solve the BVP using the finite element method and we show that we obtain the same solution given by the Ritz method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "uh = TrialFunction(Vh)\n",
    "vh = TestFunction(Vh)\n",
    "\n",
    "Aform = k*inner(grad(uh), grad(vh))*dx\n",
    "bform = inner(f,vh)*dx\n",
    "\n",
    "A,b = assemble_system(Aform, bform, bcs)\n",
    "\n",
    "uh_FE = Function(Vh)\n",
    "\n",
    "solve(A, uh_FE.vector(), b)\n",
    "\n",
    "print \"Energy functional value\", assemble(.5*k*inner(grad(uh_FE), grad(uh_FE))*dx - inner(f,uh_FE)*dx)\n",
    "\n",
    "plot(uh_FE, title=\"FE solution\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 2",
   "language": "python",
   "name": "python2"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 2
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython2",
   "version": "2.7.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
